# CPFEM_Yield_Surface

*Manipulate yield surfaces as meshed geometries.*

![img](https://i.imgur.com/y0bxwGK.gif)

A python toolbox interfaced with external codes to compute yield surface geometries on-the-fly.
This is part of the MetPlast project at [NTNU PhysMet](https://gitlab.com/ntnu-physmet)

# Use



Initialize mesh:

```
import yield_surface_mesh as ys
MyMesh = ys.YieldSurfaceMesh()
```

Generate the initial mesh structure with 100 points spread in 3D.

```
MyMesh.initial_sphere_mesh(n_total=100, n_equator=0)
```
The result directory is populated with results files named `inp<INDEX>.txt`

```
results/directory/path
├── inp1.txt
├── inp2.txt
├── ...
└── inp437.txt
```
Each file is formated with the CSV convention with columns names as header

```
# S11, S22, S33, S12, S13, S23, SDV_GAMMA
9.56204e+01,9.05459e+01,0.00000e+00,3.58711e+01,0.00000e+00,0.00000e+00,0.00000e+00
...
...
```

Then to update the mesh using stress values at a certain plastic strain level (here gamma=2e-6)
```
MyMesh.deform_mesh("SDV_GAMMA", 2e-6, 'results/directory/path')
```

### Improve the mesh density

From there we can update the data in order to refine the coarsest elements, dividing the elements with the largest "area" or "curvature" (see option below).

```
MyMesh.refine_mesh( method="area", batch_size=50, source_mesh="deformed")
```


### Use a configuration file

See `example.py` to generate a first mesh, perform a refinement and plot the result.

# Installation 

```bash
pip install -r requirements.txt
```


# TODOS

- [ ] Make logo
- [ ] Update README: clear use and explanations
- [ ] Re-structure (according to SoftwareX requirements)
- [ ] split examples into: mesh creation, mesh update, and various plots (3d_yldsurf, 3d_patterns, single_pattern)
- [ ] provide `setup.py`
