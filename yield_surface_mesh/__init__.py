"""A module for implementing the YieldSurfaceMesh class.

The idea is to represent the local evolution of a material under a given load
by its yield surface.
"""
import numpy as np
import pandas as pd
import pyvista as pv
from mayavi import mlab
from matplotlib import pyplot as plt
from matplotlib import cm as cm
# import Parallel_Coordinates_Plot as pcp


class YieldSurfaceMesh:
    """Represent materials yield surfaces in plane stress as meshes."""

    def __init__(self):
        """Perform default constructors."""
        self.nodes = None
        self.nodes_def = None
        self.elements = None

    @property
    def n_nodes(self):
        """Return len(self.nodes)."""
        return len(self.nodes)

    @property
    def n_elements(self):
        """Return len(self.elements)."""
        return len(self.elements)

    def initial_sphere_mesh(self, n_total=300, n_equator=50):
        """Generate mesh structure on a sphere.

        Parameters
        ----------
        n_total : int, optional
            Total numger of nodes
        n_equator : int, optional
            number of nodes on the equator

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        #
        points = []
        #
        if not (n_total - n_equator >= 4):
            print("Error, n_total should be larger than n_equator, ",
                  "By more than 3!")
            raise ValueError("Error: wrong input")
        else:
            # -----------------------------------------------------------------
            # Generating nodes
            # North and south pole
            points.append((0, 0, 1))
            points.append((0, 0, -1))
            # equator band half angle:
            d_theta = 2 * np.pi / n_equator if n_equator > 0 else 0
            # surface area of sphere, excluding equator band
            sphere_area = 4 * np.pi * (np.sin(np.pi / 2) - np.sin(d_theta))
            # If we divided this area into N squares of size a, size of a:
            area = np.sqrt(sphere_area / (n_total - n_equator))
            # the angle that covers cord of length a
            d_alpha = 2 * np.arcsin(area / 2)
            # Looping through these rows
            alfas = np.linspace(
                d_theta,
                np.pi / 2,
                # how many rows of such squares in each hemisphere
                int(np.round((np.pi / 2 - d_theta) / d_alpha, 0)),
                endpoint=False,
            )
            for i, alfa in enumerate(alfas):
                coord_z = np.sin(alfa)
                coord_r = np.cos(alfa)
                # Number of points in this row
                n_pts_row = np.round(2 * np.pi * coord_r / area)
                phis = np.linspace(0, 2 * np.pi, int(n_pts_row), endpoint=False)
                # shifting the points half the incremental angle everyother row
                if i % 2 != 0:
                    phis = phis + (phis[1] - phis[0]) / 2
                # shifting the points just a bit to avoid x=0 situation
                phis = phis + (phis[1] - phis[0]) / 100
                for phi in phis:
                    coord_x, coord_y = coord_r * np.cos(phi), coord_r * np.sin(phi)
                    points.append((coord_x, coord_y, coord_z))
                    # if not on the equator, add points to the south hemisphere
                    if coord_z != 0:
                        points.append((coord_x, coord_y, -1 * coord_z))
            # Equator nodes
            phis = np.linspace(0, 2 * np.pi, n_equator, endpoint=False)
            for phi in phis:
                coord_x, coord_y = np.cos(phi), np.sin(phi)
                points.append((coord_x, coord_y, 0))
            print(
                f"{len(points)} Total nodes generated, ",
                f"of them {n_equator} are on the equator.",
            )
            self.points_to_mesh(np.array(points))

    def points_to_mesh(self, points):
        """Make a mesh object from the given array of points.

        And store the nodes and elements

        Parameters
        ----------
        points : array of size(number of points, 3) of floats
            including the x, y, z coordinates of points to generate mesh from.

        Returns
        -------
        None.

        """
        # -----------------------------------------------------------------
        # Removing critical nodes
        points[np.where(points[:, 0] == 0)[0], 0] = 1e-14
        critical_indexes = iscritical(points)
        points = np.delete(points, critical_indexes, axis=0)
        # -----------------------------------------------------------------
        # Generating mesh from the nodes
        # Generating 3D volumetric mesh of kind TETRA
        # Extracting surface mesh of kind TRIANGLE from the volume
        surf = (
            pv.PolyData(points).delaunay_3d().extract_surface(nonlinear_subdivision=6)
        )
        print("Mesh Info:", surf)
        # Extracting mesh info
        # Mesh node coordinates
        mesh_points = surf.points
        n_mesh_points = len(mesh_points)
        #
        # re-arranging mesh_points so that the old points come first with the
        # same order as before re-meshing, this is important to avoid
        # recalculation.
        # grab original point indices
        point_inds = surf["vtkOriginalPointIds"]
		# there is bug in pyvista, sometimes some indexes are missing
        point_inds_fixed = np.zeros(len(point_inds))
        point_inds_fixed[point_inds.argsort()] = np.arange(len(point_inds))
        point_inds = point_inds_fixed.astype(int)
        # inversing it
        inv_point_inds = np.zeros(n_mesh_points)
        inv_point_inds[point_inds] = np.arange(n_mesh_points)
        inv_point_inds = inv_point_inds.astype(int)
        mesh_points = mesh_points[inv_point_inds]
        #
        self.nodes = pd.DataFrame(mesh_points, columns=("x", "y", "z"))
        self.nodes["tag"] = np.arange(self.n_nodes) + 1
        self.nodes_def = self.nodes.copy()
        # Mesh elements
        elems_ = surf.faces
        elems_ = elems_.reshape(int(len(elems_) / 4), 4)
        elems_ = elems_[:, 1:]
        elems = np.copy(elems_)
        for i, new_i in enumerate(inv_point_inds):
            if i != new_i:
                elems[elems_ == new_i] = i
        self.elements = pd.DataFrame(elems + 1, columns=("node1", "node2", "node3"))
        self.elements["tag"] = np.arange(self.n_elements) + 1
        self.zeroize_elements_props()

    def zeroize_elements_props(self):
        """Initialize columns that hold element properties with zero.

        Returns
        -------
        None.

        """
        self.elements["area"] = np.zeros(self.n_elements)
        self.elements["curvature"] = np.zeros(self.n_elements)
        self.elements["n1"] = np.zeros(self.n_elements)
        self.elements["n2"] = np.zeros(self.n_elements)
        self.elements["n3"] = np.zeros(self.n_elements)

    def deform_mesh(self, interpolate_param, interpolate_lvl, res_dir):
        """Update the coordinates of nodes on yield surface.

        Parameters
        ----------
        interpolate_param : string
            the parameter to be used for interpolation.
        interpolate_lvl : real
            the value for which the interpolation should be performed.
        res_dir : TYPE
            directory of result files.

        Returns
        -------
        None.

        """
        for i in range(self.n_nodes):
            # getting the interpolated output
            res_file = str(res_dir) + f"inp{str(i + 1)}.txt"
            res_interp = load_results(
                res_file,
                output_mode="interpolate",
                interp_param=interpolate_param,
                interp_range=[interpolate_lvl],
            ).loc[0]
            # Updating the coordinates of nodes
            self.nodes_def.loc[i, ["x", "y", "z"]] = [
                res_interp["S11"],
                res_interp["S22"],
                res_interp["S12"],
            ]

    def plot_mesh(
        self,
        mode="deformed",
        style="wireframe",
        surface=True,
        color_mode="random",
        update_color_field=True,
        savefig="",
        hide=False,
    ):
        """3D plot of the mesh structure using mayavi.

        Parameters
        ----------
        mode : string, optional
            shall plot the sphere points or the yield surface?
            'initial' or 'deformed'.
        style : string, optional
            plotting modes:
            "points, "wireframe", "surface", "mesh", "fancymesh"
        surface : boolean, optional
            whether to include the surface or not
        color_mode : string, optional
            coloring modes:
            'random' for random or 'area' or 'curvature'.
        update_color_field : boolean, optional
            whether to update the color field values
        savefig : string, optional
            if passed and not empty will be the name of the file to save png
        hide : boolean, optional
            whether to show or hide the figure screen


        Raises
        ------
        ValueError
            wrong input parameters for plot or coloring modes.


        """
        # dealing with the mode
        if mode == "initial":
            coord_x, coord_y, coord_z = self.nodes.x, self.nodes.y, self.nodes.z
        elif mode == "deformed":
            coord_x, coord_y, coord_z = (
                self.nodes_def.x,
                self.nodes_def.y,
                self.nodes_def.z,
            )
        else:
            raise ValueError("Error: wrong mode")
        #
        # dealing with the color mode
        if color_mode == "random":
            colorfield = np.random.rand(self.n_elements)
        elif color_mode == "area":
            if update_color_field:
                self.update_elements_area(source_mesh=mode)
            colorfield = self.elements.area.to_numpy()
        elif color_mode == "curvature":
            if update_color_field:
                self.update_elements_curvature(source_mesh=mode)
            colorfield = self.elements.curvature.to_numpy()
        else:
            raise ValueError("Error: wrong color mode")
        #
        triangles = self.elements.loc[:, ["node1", "node2", "node3"]] - 1
        opacity = 0.5 if style == "surface" else 1
        mlab.options.offscreen = hide
        mlab.figure()
        mlab.view(azimuth=90, elevation=120)
        mesh = mlab.triangular_mesh(
            coord_x,
            coord_y,
            coord_z,
            triangles,
            representation=style,
            color=(0, 0, 0),
            opacity=opacity,
        )
        if surface:
            mesh.mlab_source.dataset.cell_data.scalars = colorfield
            mesh.mlab_source.dataset.cell_data.scalars.name = "Cell data"
            mesh.mlab_source.update()
            mesh.parent.update()
            mesh2 = mlab.pipeline.set_active_attribute(mesh, cell_scalars="Cell data")
            mlab.pipeline.surface(mesh2, colormap="YlGnBu")
        if savefig != "":
            savefig += ".png"
            mlab.savefig(filename=savefig, magnification=10)

    def update_elements_area(self, source_mesh="deformed"):
        """Update element areas, for initial or deformed state of the mesh.

        Parameters
        ----------
        source_mesh : string, optional
            the state of mesh to do area calculations on.
            takes: 'deformed' or 'initial'

        Returns
        -------
        None.

        """
        for i, elem in self.elements.iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh=source_mesh)
            self.elements.loc[i, "area"] = triangle_area(vers[0], vers[1], vers[2])

    def update_elements_normal(self, source_mesh="deformed"):
        """Update element normals, for initial or deformed state of the mesh.

        Parameters
        ----------
        source_mesh : string, optional
            the state of mesh to do area calculations on.
            takes: 'deformed' or 'initial'

        Returns
        -------
        None.

        """
        for i, elem in self.elements.iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh=source_mesh)
            # computing the unit normals
            normal = plane_norm(vers[0], vers[1], vers[2])
            self.elements.loc[i, ["n1", "n2", "n3"]] = normal

    def update_elements_curvature(self, source_mesh="deformed"):
        """Update element curvature, for initial or deformed state of the mesh.

        Parameters
        ----------
        source_mesh : string, optional
            the state of mesh to do curvature calculations on.
            takes: 'deformed' or 'initial'

        Returns
        -------
        None.

        """
        # first, updating the element normals
        self.update_elements_normal(source_mesh=source_mesh)
        # second, looping through all elements and finding the average angle
        # between each element and it's neighbours (2 nodes in common)
        for i, elem in self.elements.iterrows():
            # finding all other elements that have same nodes as this one
            node_tags = elem[["node1", "node2", "node3"]].to_numpy()
            # finding the neighbour elems, with more than one node in common
            querytxt = (
                "(node1 in @node_tags & node2 in @node_tags)"
                + " or "
                + "(node1 in @node_tags & node3 in @node_tags)"
                + " or "
                + "(node2 in @node_tags & node3 in @node_tags)"
            )
            neighbours = self.elements.query(querytxt).index
            # removing the current element itself
            neighbours = np.delete(neighbours, np.where(neighbours == i))
            # average angle between element and its neighbours
            angles = np.zeros(3)
            current_elem_normal = self.elements.loc[i, ["n1", "n2", "n3"]]
            for indx, neighb_id in enumerate(neighbours):
                angles[indx] = angle_between(
                    self.elements.loc[neighb_id, ["n1", "n2", "n3"]],
                    current_elem_normal,
                    sup_angle=True,
                )
            self.elements.loc[i, "curvature"] = np.average(angles)

    def refine_mesh(self, method="curvature", batch_size=10, source_mesh="deformed"):
        """Refine mesh.

        By adding new nodes to the center of first {batch_size} large elements
        or elements with highest curvature and then remeshing.

        Parameters
        ----------
        method : refining method
            takes: "curvature" or "area"
        batch_size : integer, optional
            the number of new nodes to add.
        source_mesh : string, optional
            the state of mesh to do refining on.
            takes: 'deformed' or 'initial'

        Returns
        -------
        None.

        """
        #print("----------------- Refining -----------------")
        if method == "curvature":
            self.update_elements_curvature(source_mesh=source_mesh)
            # Sorting by curvature
            self.elements = self.elements.sort_values(by="curvature", ascending=False)
        elif method == "area":
            self.update_elements_area(source_mesh=source_mesh)
            # Sorting by area
            self.elements = self.elements.sort_values(by="area", ascending=False)
        else:
            raise ValueError("Error: wrong refining method")

        # Selecting the first -batch_size- elememnts
        new_points = []
        for _, elem in self.elements.head(batch_size).iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh="initial")
            # finding centroid of the triangular element
            centroid = np.sum(vers, axis=0) / 3
            centroid *= 1 / np.sqrt(
                centroid[0] ** 2 + centroid[1] ** 2 + centroid[2] ** 2
            )
            new_points.append((centroid[0], centroid[1], centroid[2]))

        new_points = np.array(new_points)
        print(str(len(new_points)) + " refining nodes were added!")
        points = self.nodes.loc[:, ["x", "y", "z"]].to_numpy()
        # Adding the new nodes to the exisiting nodes
        points = np.concatenate((points, new_points), axis=0)
        self.points_to_mesh(points)
        #print("----------------- Done Refining! -------------------")

    def refine_region(self, axis, phi1, phi2, batch_size=10, source_mesh="deformed"):
        """Refine mesh based on the element area, only for a certain region.

        the region is defined by an axis passing through the origin, and two
        angles phi1 and phi2 where phi1<phi2 and both angles are in range
        [0,pi]. All elements inside or crossing the region will be considered
        for refining.

        Parameters
        ----------
        axis : vector of size 3 of floats
            the vector which defines the refining region.
        phi1 : float
            first angle that defines the refining region.
        phi2 : float
            second angle that defines the refining region.
        batch_size : integer, optional
            the number of new nodes to add.
        source_mesh : string, optional
            the state of mesh to do refining on.
            takes: 'deformed' or 'initial'

        Returns
        -------
        None.

        """
        axis = unit_vector(axis)
        in_region_elem_tags = []
        # checking if transformation is required
        if (axis != [0, 0, 1]).any():
            # axis and angle of rotation that takes the give axis to Z axis
            phi = -1 * angle_between(axis, [0, 0, 1], unit="rad")
            rot_vec = np.cross(axis, [0, 0, 1])

        for i, elem in self.elements.iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh=source_mesh)
            # checking if transformation is required
            if (axis != [0, 0, 1]).any():
                for j in range(3):
                    # transformation, rotating vertice
                    vers[j] = rotate_along_vec(vers[j], rot_vec, phi)
            # angles with Z axis
            thetas = np.zeros(3)
            for j in range(3):
                thetas[j] = angle_between(vers[j], [0, 0, 1])
            # an element is assumed to be inside or intersect with the region
            # of interest unless the whole element is either below or above the
            # region. this is realized by looking at the angle nodes makes with
            # z axis, and comparing with angles that define the region.
            in_region = True
            if (max(thetas) < phi1) or (min(thetas) > phi2):
                in_region = False
            if in_region:
                in_region_elem_tags.append(elem.tag)
        if len(in_region_elem_tags) == 0:
            # No refining node was detected, the range is too small to contain
            # or cross any element because it is completely inside one element
            i_s, intersection = self.find_intersecting_elem(np.zeros(3), axis, source_mesh="deformed")
            if len(i_s) != 0:
                for i_si in i_s:
                    in_region_elem_tags.append(i_s[0])
            else:
                raise ValueError("Error: Can't find any intersection!")
        in_region_elems = self.elements.loc[self.elements["tag"].isin(in_region_elem_tags)]
        self.update_elements_curvature(source_mesh=source_mesh)
        in_region_elems = in_region_elems.sort_values(by="curvature", ascending=False)
        # Selecting the first -batch_size- elememnts
        new_points = []
        for _, elem in in_region_elems.head(batch_size).iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh="initial")
            # finding centroid of the triangular element
            centroid = np.sum(vers, axis=0) / 3
            centroid *= 1 / np.sqrt(
                centroid[0] ** 2 + centroid[1] ** 2 + centroid[2] ** 2
            )
            new_points.append((centroid[0], centroid[1], centroid[2]))
        new_points = np.array(new_points)
        print(str(len(new_points)) + " refining nodes were added")
        points = self.nodes.loc[:, ["x", "y", "z"]].to_numpy()
        # Adding the new nodes to the exisiting nodes
        points = np.concatenate((points, new_points), axis=0)
        self.points_to_mesh(points)
        #print("----------------- Done Refining! -------------------")

    def find_angle_in_deformed_mesh(self, z_level):
        """Find the angle from Z axis corresponding to a Z value.

        this functions returns the angle of the loading direction with Z axis,
        that gives the closest Z value to a given Z value. used for refining
        mesh where the surface cuts the Z=z_level surface

        Parameters
        ----------
        z_level : float
            given z value.

        Returns
        -------
        angle : float
            angle of direction with Z axis.

        """
        # finding the node with closest z to z_level in deformed mesh
        closest_level = self.nodes_def.iloc[
            (self.nodes_def["z"] - z_level).abs().argsort()[0]
        ]
        closest_level = closest_level[["x", "y", "z"]].to_numpy()
        # computing the angle
        angle = angle_between(closest_level, [0, 0, 1])
        return angle

    def find_intersecting_elem(self, l_1, l_2, source_mesh="deformed"):
        """Find the elements that cross a direction defined by two points.

        Parameters
        ----------
        l_1, l_2: Vectors of size 3 of float
            coordinates of the points that define a direction
        source_mesh : string, optional
            the state of mesh to extract coordinates of element from.
            takes: 'deformed' or 'initial'

        Returns
        -------
        vector of size n of integers
            tags of the elements that cross with the given direction.

        """
        found_elems_tags = []
        found_elems_tags2 = []
        for _, elem in self.elements.iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh=source_mesh)
            # finding the intersection point
            intersection, in_direction = line_plane_intersect(
                vers[0], vers[1], vers[2], l_1, l_2
            )
            # checking if intersection exists
            if not (intersection is None) > 0:
                # there should be two answers, one is wrong because it is
                # in the opposite direction. checking it:
                if in_direction:
                    # checking if intersection is inside the element
                    # by looking at the sum of angles between lines connecting
                    # the intersection to the vertexes, if its equal to 2pi
                    thetas = np.zeros(3)
                    for j_0 in range(3):
                        j_1 = j_0 + 1 if j_0 != 2 else 0
                        if (np.linalg.norm(intersection - vers[j_0])<1e-12) or \
                           (np.linalg.norm(intersection - vers[j_1])<1e-12):
                               thetas = np.array([360,0,0])
                        else:
                            thetas[j_0] = angle_between(
                                intersection - vers[j_0], intersection - vers[j_1]
                            )
                    if sum(thetas) >= (360 - 1e-6):
                        found_elems_tags.append(elem.tag)
                    # there is a small chance that the direction hits exactly
                    # a node so the criteria above doesn't work, we save all
                    # such cases.
                    for j in range(3):
                        if sum(np.absolute(intersection - vers[j])) < 1e-12:
                            found_elems_tags2.append(elem.tag)

        if len(found_elems_tags) == 0:
            print("the direction doesn't intersect any element, but hits a node")
            found_elems_tags = found_elems_tags2
        print("number of intersections:", len(found_elems_tags))
        return np.array(found_elems_tags), intersection

    def normal_in_direction(self, direction, source_mesh="deformed"):
        """Find normal to the surface where it intersects a given direction.

        Parameters
        ----------
        direction : vector of size 3 of floats
            defining a direction.
        source_mesh : string, optional
            the state of mesh to extract coordinates of element from.
            takes: 'deformed' or 'initial'

        Returns
        -------
        normal : vector of size 3 of floats
            normal to the surface where it intersects with the given direction

        """
        # finding tags of elements that cross a given direction
        tags, intersection = self.find_intersecting_elem(np.zeros(3), direction)
        elems = self.elements.loc[self.elements["tag"].isin(tags)]
        normals = []
        for _, elem in elems.iterrows():
            # finding the vertexes
            vers = self.element_vertices(elem.tag, source_mesh=source_mesh)
            # finding the normal
            normal = plane_norm(vers[0], vers[1], vers[2])
            normal = unit_vector(normal)
            # equation of plane: Ax+By+Cz=D where A,B,C are the normals and D:
            D = normal[0]*intersection[0] + normal[1]*intersection[1] + normal[2]*intersection[2]
            # a point is in the same side of the normal if Ax+By+Cz-D>0, we want
            # the origin to be inside, so opposite side of normal, so:
            if -1*D>0:
                normal = normal * -1
            normals.append(normal)
        return np.average(np.array(normals), 0)

    def element_vertices(self, element_tag, source_mesh="deformed"):
        """Return vertex coordinates of an element.

        Parameters
        ----------
        element_tag : integer
            element tag
        source_mesh : string, optional
            the state of mesh to extract coordinates of element from.
            takes: 'deformed' or 'initial'

        Returns
        -------
        vers : vector of size 3 of floats
            coordinates of the vertexes of the element

        """
        elem = self.elements[self.elements.tag == element_tag]
        node_tags = elem[["node1", "node2", "node3"]].to_numpy()[0]
        if source_mesh == "deformed":
            node = self.nodes_def[self.nodes_def.tag.isin(node_tags)]
        elif source_mesh == "initial":
            node = self.nodes[self.nodes.tag.isin(node_tags)]
        else:
            raise TypeError("Error: wrong mode")
        vers = node[["x", "y", "z"]].to_numpy()
        return vers


def triangle_area(p_1, p_2, p_3):
    """Calculate the area of a triangle.

    Using its vertex coordinates in the Cartesian coordinate system.

    Parameters
    ----------
    p_1, p_2, p_3: Vectors of size 3 of float
        coordinates of the corners of the triangle

    Returns
    -------
    TYPE : float
        area of the triangle.

    """
    # First Calculating edge length
    e_1 = np.sqrt(
        (p_1[0] - p_2[0]) ** 2 + (p_1[1] - p_2[1]) ** 2 + (p_1[2] - p_2[2]) ** 2
    )
    e_2 = np.sqrt(
        (p_1[0] - p_3[0]) ** 2 + (p_1[1] - p_3[1]) ** 2 + (p_1[2] - p_3[2]) ** 2
    )
    e_3 = np.sqrt(
        (p_3[0] - p_2[0]) ** 2 + (p_3[1] - p_2[1]) ** 2 + (p_3[2] - p_2[2]) ** 2
    )
    # Using the Heron formula to find the triangle area.
    s_heron = (e_1 + e_2 + e_3) / 2
    return np.sqrt(s_heron * (s_heron - e_1) * (s_heron - e_2) * (s_heron - e_3))


def plane_norm(p_1, p_2, p_3):
    """Return normal of a plane passing through three points.

    Parameters
    ----------
    p_1 : vector of size 3 of float
        first point.
    p_2 : vector of size 3 of float
        second point.
    p_3 : vector of size 3 of float
        third point.

    Returns
    -------
    vector of size 3 of float
        normal of the plane passing through these points.

    """
    return unit_vector(np.cross(p_2 - p_1, p_3 - p_1))


def unit_vector(vector):
    """Return the unit vector of the vector.

    Parameters
    ----------
    vector : vector of size 3 of float
        input vector

    Returns
    -------
    vector of size 3 of float
        unit vector of the input vector

    """
    return vector / np.linalg.norm(vector)


def angle_between(vec1, vec2, sup_angle=False, unit="deg"):
    """Return the angle in degrees between vectors 'v1' and 'v2'.

    Parameters
    ----------
    vec1 : vector of size 3 of float
        first vector
    vec2 : vector of size 3 of float
        second vector.
    sup_angle: boolean, optional
    if True the supplementary angle between two vectors will be returned,
    therefore always less than 90 degrees.
    unit: string, optional
    the unit of the angle to compute

    Returns
    -------
    float
        angle between two vectors in degrees.

    """
    dot_product = np.dot(unit_vector(vec1), unit_vector(vec2))

    if (-1 > dot_product) or (dot_product > 1):
        dot_product = np.sign(dot_product)
    if sup_angle:
        angle = np.arccos(np.abs(dot_product))
    else:
        angle = np.arccos(dot_product)
    if unit == "deg":
        angle = np.rad2deg(angle)
    return angle


def rotate_along_vec(point, vec, theta):
    """Rotate the give point p0, theta degress along the give vector U.

    Parameters
    ----------
    point : vector of size 3 of floats
        the coordinates of the point to rotate.
    vec : vector of size 3 of floats
        the vector which rotation is about.
    theta : float
        rotation angle in radian.

    Returns
    -------
    vector of size 3 of floats
        the coordinates of the rotated point.

    """

    def matrix_multiply(*matrices):
        if len(matrices) == 1:
            return matrices
        else:
            m_other = matrix_multiply(*matrices[1:])
            return np.matmul(matrices[0], m_other)

    point = np.concatenate((point, [1])).reshape(4, 1)
    vec = unit_vector(vec)
    d_v = np.sqrt(vec[1] ** 2 + vec[2] ** 2)

    if d_v < 1e-16:
        r_x = np.identity(4)
        r_x_inv = np.identity(4)
    else:
        r_x = [
            [1, 0, 0, 0],
            [0, vec[2] / d_v, -vec[1] / d_v, 0],
            [0, vec[1] / d_v, vec[2] / d_v, 0],
            [0, 0, 0, 1],
        ]
        r_x_inv = [
            [1, 0, 0, 0],
            [0, vec[2] / d_v, vec[1] / d_v, 0],
            [0, -vec[1] / d_v, vec[2] / d_v, 0],
            [0, 0, 0, 1],
        ]

    r_y = [[d_v, 0, -vec[0], 0], [0, 1, 0, 0], [vec[0], 0, d_v, 0], [0, 0, 0, 1]]
    r_y_inv = [[d_v, 0, vec[0], 0], [0, 1, 0, 0], [-vec[0], 0, d_v, 0], [0, 0, 0, 1]]

    c_t = np.cos(theta)
    s_t = np.sin(theta)
    r_z = [[c_t, s_t, 0, 0], [-s_t, c_t, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    point_rotated = matrix_multiply(r_x_inv, r_y_inv, r_z, r_y, r_x, point)
    return np.reshape(point_rotated[0][:3], (3,))


def load_results(
        res_file, output_mode="original", interp_param="SDV_WP",
        interp_range=[10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6]):
    """Load results from text file that holds abaqus run results and export.

    either the whole results for all frames or interpolated results at the
    given levels for the given varaible.

    Parameters
    ----------
    res_file : string
        name of the text file with results.
    output_mode : string, optional
        takes "original" or "interpolate". The default is "original".
    interp_param : string, optional
        the name of the variable based on which the interpolation is intened.
        The default is "SDV_WP".
    interp_range : vector of floats, optional
        the levels at which interpolation is requested.
        The default is [10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6].

    Raises
    ------
    ValueError
        wrong mode.

    Returns
    -------
    array of shape (# of frames, # of stored variables) of floats or
    array of shape (# of levels, # of stored variables) of floats, depending
    on the requsted mode.
        results loaded from the give file.

    """
    # Import data
    data = np.genfromtxt(res_file, delimiter=",", names=True)
    # Convert data into pd dataframe
    res = pd.DataFrame(data[:][0:], columns=data.dtype.names)

    if output_mode == "original":
        return res
    if output_mode == "interpolate":
        # ---------------------------------------------------------------------
        # Linear interpolation to find the new values at requested levels
        n_cols = len(res.columns)
        param = res[interp_param]
        new_res = np.zeros((len(interp_range), n_cols))
        for i in range(n_cols):
            new_res[:, i] = np.interp(interp_range, param, res.to_numpy()[:, i])
        # ---------------------------------------------------------------------
        new_res = pd.DataFrame(new_res, columns=res.columns)
        # Checking if the interpolation was successful
        mx, mn =max(new_res[interp_param]), min(new_res[interp_param])
        if (mn != min(interp_range)) or (mx != max(interp_range)):
            print("interpolation failed for the following case: ",
                  res_file.split("/")[-1])
            if (mn != min(interp_range)):
                print(f"min in range:{min(interp_range)},",
                      f"min acheived {mn}")
            if (mx != max(interp_range)):
                print(f"max in range:{max(interp_range)},",
                      f"max acheived {mx}")
        return new_res
    # Else there is a problem
    raise ValueError("Error: wrong output_mode")


def plot_xy(data,
            xvar, yvars, xlabel="", ylabel="",
            interp_data=[], color_map="jet", marker="",
            slope=None,
            yratios=[],
            is_xlog=False):
    """Plot yvars-vs-xvar curves for a certain node.

    Parameters
    ----------
    data : array of shape (# of frames, # of stored variables) of floats
        the results generated from abaqus, for all the tested nodes.
    xvar : string
        the name of the variable to plot on the X axis
    yvars : array of strings
        the name of the variables to plot on the Y axis
    xlabel : string, optional
        the label to use for x-axis when plotting
    ylabel : string, optional
        the label to use for y-axis when plotting
    interp_data : array of shape (# of levels, # of stored variables) of floats
        optional, the interpolated data
    color_map : string, optional
        the name of colormap to use, default is "jet"
    marker : string, optional
        marker option for the plot
    slope : float, optional
        if provided, lines connecting the interpolation points to x-axis will
        be plotted for the first yvar, using this slope.
    yratios : array of strings, optional
        if provided, yratios-vs-xvar curves will be plotted

    Returns
    -------
    None.
    """
    _, ax1 = plt.subplots(figsize=(8, 6))
    cmap = cm.get_cmap(color_map)
    if xlabel == "":
        xlabel = xvar
    labels = ["$\sigma_{11}$","$\sigma_{22}$","$\sigma_{33}$","$\sigma_{12}$","$\sigma_{13}$","$\sigma_{23}$"]
    for i, yvar in enumerate(yvars):
        lbl = "$\sigma_{11}$"
        color = cmap(i/len(yvars))
        ax1.plot(data[xvar], data[yvar],
                 color=color, marker=marker, label=labels[i]) # label = yvar
        if len(interp_data) != 0:
            ax1.scatter(interp_data[xvar], interp_data[yvar],
                        marker="o", s=30, color='black')
    if slope is not None:
        for i in range(len(interp_data[xvar])):
            interp_x, interp_y = interp_data[xvar][i], interp_data[yvars[0]][i]
            x_on_xaxis = interp_x - interp_y/slope
            print(f"point{i+1}, on X-axis gives: {x_on_xaxis}")
            ax1.plot([interp_x, x_on_xaxis], [interp_y, 0],
                     linestyle='dashed', color='black')
    ax1.set_xlabel("Simulation time (s)") # xlabel
    ax1.set_ylabel(ylabel)
    if is_xlog:
        ax1.set_xscale("log")
    ax1.legend()
    #
    if len(yratios) != 0:
        _, ax2 = plt.subplots(figsize=(8, 6))
        for i, yratio in enumerate(yratios):
            color = cmap(i/len(yratios))
            yvar1, yvar2 = yratio.split("/")
            ax2.plot(data[xvar], data[yvar1]/data[yvar2],
                     color=color, marker=marker, label=yratio)
        ax2.set_xlabel(xlabel)
        ax2.set_ylabel("Ratios")
        if is_xlog:
            ax2.set_xscale("log")
        ax2.legend()
    #
    plt.show()


def plot_3d_cloud(res0, res, coloring_param="SDV_WP", mode="levels",
                  single_path_ids=[], viewport=""):
    """Plot 3D point cloud of datapoints.

    Parameters
    ----------
    res0 : array of shape (# of nodes, # of frames, # of stored variables)
        of floats
        the results generated from abaqus, for all the tested nodes.
    res : array of shape (# of nodes, # of levels, # of stored variables)
        of floats
        the results generated from abaqus, for all the tested nodes,
        interpolated at certain (plastic strain or work) levels
    coloring_param : string, optional
        the name of the variable to use for coloring the points
        The default is "SDV_WP".
    mode : string, optional
        whether to plot only the datapoints at interpolated levels, or all
        original results or both.
        takes "all", "levels", "both". The default is "levels".

    Returns
    -------
    None.

    """
    if viewport == "xy":
        elev, azim = 90, -90
    if viewport == "xz":
        elev, azim = 0, -90
    if viewport == "yz":
        elev, azim = 0, 180
    cmap = cm.get_cmap("jet")
    # Finding the index of columns for the desired stress components
    # as well as the parameter that is used to define point colors
    clmns = res0[0].columns.to_numpy()
    sorter = np.argsort(clmns)
    col_names = ["S11", "S22", "S12", coloring_param]
    col_indexes = sorter[np.searchsorted(clmns, col_names, sorter=sorter)]
    if mode in ("all", "both"):
        res0 = np.array(res0)
        n_frames = res0.shape[1]
        # Selecting only S11,S22,S12 from results
        sig = res0[:, :, col_indexes[0:3]]
        # Coloring points by:
        clr_vals = res0[:, :, col_indexes[3]]
        #
        # 3D plot of All points, cloud of points
        fig1 = plt.figure(figsize=(8, 8))
        ax1 = fig1.add_subplot(111, projection="3d", proj_type='ortho')
        for i in range(n_frames):
            ax1.scatter(
                sig[:, i, 0],
                sig[:, i, 1],
                sig[:, i, 2],
                s=10,
                color=cmap(clr_vals[:, i] / np.max(clr_vals)),
                alpha=0.5
            )
        if viewport != "":
            ax1.elev, ax1.azim = elev, azim
        set_axes_equal(ax1)
        #ax1.set_xlim(0,140)
        #ax1.set_ylim(-100,40)
    #
    if mode in ("levels", "both"):
        res = np.array(res)
        n_levels = res.shape[1]
        # Selecting only S11,S22,S12 from results
        sig = res[:, :, col_indexes[0:3]]
        # Coloring points by:
        clr_vals = res[:, :, col_indexes[3]]
        #
        # 3D plot of level only, cloud of points
        fig2 = plt.figure(figsize=(8, 8))
        ax2 = fig2.add_subplot(111, projection="3d", proj_type='ortho')
        for i in range(n_levels):
            ax2.scatter(
                sig[:, i, 0],
                sig[:, i, 1],
                sig[:, i, 2],
                s=5,
                label=f"{clr_vals[0,i]:.6f}",
            )
        for s_id in single_path_ids:
            #if ((sig[s_id, :, 2])>=0).all():
            ax2.plot(sig[s_id, :, 0],
                     sig[s_id, :, 1],
                     sig[s_id, :, 2],
                     color="black")
        ax2.legend()
        if viewport != "":
            ax2.elev, ax2.azim = elev, azim
        set_axes_equal(ax2)

def plot_section(res, elements, which_lvls=None, section_at=0, mode="3D",
                 interp_param="", interp_range=[]):
    """Plot yield surface for an ISO-S12 section in 2D or 3D.

    Parameters
    ----------
    res : array of shape (# of nodes, # of levels, # of stored variables)
        of floats
        the results generated from abaqus, for all the tested nodes,
        interpolated at certain (plastic strain or work) levels
    elements : pd dataframe
        mesh information as pd fataframe, used to identify those elements that
        cut the desired ISO-S12 plane.
    which_lvls : Array of size levels, zeros or ones, optional
        defines which of the levels should be plotted or ignored
    section_at : float, optional
        the ISO-S12 value for which plotting is requsted. The default is 0.
    mode : string, optional
        defines whether the 3D or 2D plot is desired. the 3D plot also includes
        interpolation lines and elements that are used for getting values at
        S12=section_at
        takes: "3D" or "2D". The default is "3D".
    interp_param : string, optional
        the name of the variable for which the iso sections are calculated.
        only for showing on the plot
    interp_range : vector of floats, optional
        the levels at which the interpolation has been performed.
        only for showing on the plot

    Raises
    ------
    ValueError
        plot mode is wrong.

    Returns
    -------
    None.

    """
    if mode == "3D":
        fig1 = plt.figure(figsize=(8, 8))
        ax1 = fig1.add_subplot(111, projection="3d")
    elif mode == "2D":
        fig1, ax1 = plt.subplots(figsize=(8, 6))
    else:
        raise ValueError("Error: wrong mode")

    # Finding the index of columns for the desired stress components
    clmns = res[0].columns.to_numpy()
    sorter = np.argsort(clmns)
    stresses = ["S11", "S22", "S12"]
    s_indexes = sorter[np.searchsorted(clmns, stresses, sorter=sorter)]
    res = np.array(res)
    n_levels = res.shape[1]
    # default option is to plot all the levels, unless provided
    if which_lvls is None:
        which_lvls = np.ones(n_levels)

    # Selecting only S11,S22,S12 from results
    sig = res[:, :, s_indexes]
    for ilvl in range(n_levels):
        if which_lvls[ilvl] == 1:
            points = []
            xyz = sig[:, ilvl, :]
            for _, tags in elements.iterrows():
                node_tags = tags.loc[["node1", "node2", "node3"]].to_numpy()
                node_tags = node_tags.astype(int) - 1
                # Selecting elements that cut the section plane
                xyzi = xyz[node_tags, :]
                if np.min(xyzi[:, 2]) <= section_at <= np.max(xyzi[:, 2]):
                    indx = np.zeros(3).astype(int)
                    equal_index = np.where(xyzi[:, 2] == section_at)[0]
                    if len(equal_index) > 0:
                        # 1 or 2 of the edges of triangle is on intersection!
                        t_1, t_2 = 0, len(equal_index) - 1
                        p_1 = xyzi[equal_index[t_1], [0, 1]]
                        p_2 = xyzi[equal_index[t_2], [0, 1]]
                        i_1, i_2, i_3 = 0, 0, 0
                    else:
                        # find intersections
                        s_1 = np.where(xyzi[:, 2] > section_at)[0]
                        s_2 = np.where(xyzi[:, 2] < section_at)[0]
                        if len(s_1) == 1:
                            indx[0], indx[1:] = s_1, s_2
                        else:
                            indx[0], indx[1:] = s_2, s_1
                        i_1, i_2, i_3 = (
                            [indx[0], indx[1]],
                            [indx[0], indx[2]],
                            [indx[1], indx[2]],
                        )
                        p_1 = line_zplane_intersect(
                            xyzi[indx[0], :], xyzi[indx[1], :], section_at
                        )
                        p_2 = line_zplane_intersect(
                            xyzi[indx[0], :], xyzi[indx[2], :], section_at
                        )
                    points.append(p_1)
                    points.append(p_2)

                    if mode == "3D":
                        ax1.plot(
                            [p_1[0], p_2[0]],
                            [p_1[1], p_2[1]],
                            [section_at, section_at],
                            color="red",
                        )
                        if not (i_1 == i_2 == i_3 == 0):
                            ax1.plot(xyzi[i_1, 0], xyzi[i_1, 1], xyzi[i_1, 2], "b-")
                            ax1.plot(xyzi[i_2, 0], xyzi[i_2, 1], xyzi[i_2, 2], "b-")
                            ax1.plot(xyzi[i_3, 0], xyzi[i_3, 1], xyzi[i_3, 2], "g-")

            if mode == "2D":
                new_label = [2, 10, 50, 200, 1000, 2000]
                points = np.array(points)
                ax1.scatter(points[:, 0], points[:, 1], c='r', s=3)
                # sorting points
                x_sorted, y_sorted = sort_xy(points[:, 0], points[:, 1])
                label = ""
                if interp_param != "" and len(interp_range) >= 0:
                    #label = interp_param + "=" + str(interp_range[ilvl])
                    label = r"$\epsilon_p$= " + f"{new_label[ilvl]:d}" + r"$\mu$"
                ax1.plot(x_sorted, y_sorted, label=label)
                ax1.set_aspect("equal", adjustable="box")
                ax1.set_xlabel(r"$\sigma_{11}$")
                ax1.set_ylabel(r"$\sigma_{22}$")
                ax1.set_xlim(-60,60)
                ax1.set_ylim(-60,60)
                #ax1.set_title(r"$\sigma_{12}=$" + str(section_at))
                ttle = "Sections of ISO plastic work yield surfaces at " + r"$\sigma_{12}=$" + str(section_at)
                ax1.set_title(ttle)
                if interp_range != []:
                    ax1.legend(loc="upper left", prop={'size': 16})


def sort_xy(x_unsorted, y_unsorted):
    """Counterclockwise sorting of x, y data.

    dependent on the angle between the line from the points and the center of
    mass of the whole polygon and the horizontal line.
    from user SariO in stackoverflow

    Parameters
    ----------
    x_unsorted : 1D array of floats
        unsorted x coordinates.
    y_unsorted : 1D array of floats
        unsorted y coordinates.

    Returns
    -------
    x_sorted : 1D array of floats
        sorted x coordinates.
    y_sorted : 1D array of floats
        sorted y coordinates.
    """
    x_mean = np.mean(x_unsorted)
    y_mean = np.mean(y_unsorted)

    radius = np.sqrt((x_unsorted - x_mean) ** 2 + (y_unsorted - y_mean) ** 2)

    angles = np.where(
        (y_unsorted - y_mean) > 0,
        np.arccos((x_unsorted - x_mean) / radius),
        2 * np.pi - np.arccos((x_unsorted - x_mean) / radius),
    )

    mask = np.argsort(angles)

    x_sorted = x_unsorted[mask]
    y_sorted = y_unsorted[mask]

    x_sorted = np.concatenate((x_sorted, [x_sorted[0]]))
    y_sorted = np.concatenate((y_sorted, [y_sorted[0]]))

    return x_sorted, y_sorted


def line_zplane_intersect(p_1, p_2, sec):
    """Find the intersection of a line passing two points with Z=sec plane.

    Parameters
    ----------
    p_1 : vector of size 3 of floats
        first point
    p_2 : vector of size 3 of floats
        second point.
    sec : float
        defines the intersection plane as Z=sec

    Returns
    -------
    array of size 2 of floats
        intersection point on the Z=sec plane

    """
    pars = p_1 - p_2
    time = (sec - p_1[2]) / pars[2]
    return [p_1[0] + time * pars[0], p_1[1] + time * pars[1]]


def line_plane_intersect(p_1, p_2, p_3, l_1, l_2):
    """Find the intersection of a line passing through 2 points with a plane.

    the plane is defined by three points

    Parameters
    ----------
    p_1, p_2, p_3: Vectors of size 3 of float
        coordinates of the points defining the plane
    l_1, l_2: Vectors of size 3 of float
        coordinates of the points defining the line


    Returns
    -------
    intersection: array of size 3 of floats
        coordinates of the intersection point
    in_direction: boolean
        returns True if the intersection is in the direction from l_1 to l_2,
        and False if otherwise.

    """
    normal = plane_norm(p_1, p_2, p_3)
    line = l_2 - l_1

    ndotu = normal.dot(line)

    if abs(ndotu) < 1e-6:
        intersection = None
        in_direction = None
    else:
        w_line = l_1 - p_1
        s_i = -normal.dot(w_line) / ndotu
        intersection = w_line + s_i * line + p_1
        # finding the direction
        in_direction = True
        # two vectors have the same direction if their unit vectors are equal
        if (
            sum(np.absolute(unit_vector(intersection - l_1) - unit_vector(l_2 - l_1)))
            > 1e-8
        ):
            # opposite directions!
            in_direction = False
    return intersection, in_direction


def iscritical(points):
    """Return the ids of the points with critical ratios.

    Parameters
    ----------
    points : array of size (n,3) of floats
        mesh nodes.

    Returns
    -------
    indexes : vector of integers
        ids of the points with critical ratios.

    """
    #
    ratio2 = points[:, 1] / points[:, 0]
    ratio4 = points[:, 2] / points[:, 0]
    indexes1 = np.where(np.abs(ratio2) > 1e16)[0]
    indexes2 = np.where(np.abs(ratio4) > 1e16)[0]
    indexes = np.intersect1d(indexes1, indexes2)
    if len(indexes) > 0:
        print(f"{len(indexes)} nodes cause critical loading ratios.")
    return indexes
def set_axes_equal(ax: plt.Axes):
    """Set 3D plot axes to equal scale.

    Make axes of 3D plot have equal scale so that spheres appear as
    spheres and cubes as cubes.  Required since `ax.axis('equal')`
    and `ax.set_aspect('equal')` don't work on 3D.
    """
    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])
    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    _set_axes_radius(ax, origin, radius)

def _set_axes_radius(ax, origin, radius):
    x, y, z = origin
    ax.set_xlim3d([x - radius, x + radius])
    ax.set_ylim3d([y - radius, y + radius])
    ax.set_zlim3d([z - radius, z + radius])

# def plot_parallel(res):
#     #raise NotImplementedError('Requires PCP, another homemade module')
#     list_names = ["S11", "S22", "S33", "S12", "S13", "S23"]
#     sig = res[list_names].to_numpy()
#     pcp.Parallel_plot(sig, lables=list_names, title="Stress", scale=True, mode="PC")