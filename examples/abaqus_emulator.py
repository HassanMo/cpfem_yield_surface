"""A module for emulating abaqus.

A simple von-mises based solution with voce hardening is implemented to
showcase the yield_surface_mesh class features.
"""
import os
import numpy as np
from matplotlib import pyplot as plt

def voce_extended(gamma):
    """Return  the reference stress of the eponymous function

    Parameters
    ----------
    gamma : float or list of floats

    Returns
    -------
    equivalent_stress : float or list of floats

    """
    # for 6061-T6
    tau0, tau1, theta0, theta1 = 112, 13, 230, 17
    return tau0 + (tau1 + theta1 * gamma) * (1 - np.exp(-gamma * theta0 / tau1))


def mises(stress):
    """Return the vonMises equivalent scalar"""
    hydrostatic = np.identity(3) * np.trace(stress) / 3
    deviatoric = stress - hydrostatic
    return np.sqrt(3 / 2) * np.linalg.norm(deviatoric)


def hosford0(sig, a):
    """Return the effective stress of the Hosford Model as scalar.
    using the principal stresses
    """
    vm_stress = mises(sig)
    eigenvalues, eigenvectors = np.linalg.eig(sig)
    ps = eigenvalues / vm_stress
    phi = vm_stress * (0.5 * (
                            (np.abs(ps[0]-ps[1]))**a +
                            (np.abs(ps[0]-ps[2]))**a +
                            (np.abs(ps[1]-ps[2]))**a
                            )) ** (1/a)
    return phi
def hosford(sig, a, gradient = False):
    """Return the effective stress of the Hosford Model as scalar.
    And gradient as a (6x1) vector
    Using the stress components
    """
    A = sig[1, 1] - sig[2, 2]  # S22-S33
    B = sig[2, 2] - sig[0, 0]  # S33-S11
    C = sig[0, 0] - sig[1, 1]  # S11-S22
    F = sig[1, 2]  # S23
    G = sig[0, 2]  # S13
    H = sig[0, 1]  # S12
    I2 = (1/3) * (F**2 + G**2 + H**2) + \
         (1/54) * ((A-C)**2 + (C-B)**2 + (B-A)**2)
    I3 = (1/54) * ((C-B) * (A-C) * (B-A)) + F*G*H + \
         (-1/6) * ((C-B)*F**2 + (A-C)*G**2 + (B-A)*H**2)
    tetha = np.arccos(np.round(I3 / I2 ** (3/2),10))
    psi1 = (3*I2)**(a/2)
    psi2 = (2*np.cos((2*tetha + np.pi)/6))**a + \
           (2*np.cos((2*tetha - 3*np.pi)/6))**a + \
           (-2*np.cos((2*tetha + 5*np.pi)/6))**a
    psi = psi1 * psi2
    phi = (psi / 2) ** (1/a)
    if gradient:
        # dXds = [S11, S22, S33, S12, S13, S23]
        dA = np.array([0, 1, -1, 0, 0, 0])
        dB = np.array([-1, 0, 1, 0, 0, 0])
        dC = np.array([1, -1, 0, 0, 0, 0])
        dF = np.array([0, 0, 0, 0, 0, 1])
        dG = np.array([0, 0, 0, 0, 1, 0])
        dH = np.array([0, 0, 0, 1, 0, 0])
        dI2 = (2/3) * (F*dF + G*dG + H*dH) + \
            (2/54) * ((A-C)*(dA-dC) + (C-B)*(dC-dB) + (B-A)*(dB-dA))
        dI3 = (1/54) * ((dC-dB)*((A-C)*(B-A)) + (C-B)*((dA-dC)*(B-A) + (A-C)*(dB-dA))) + \
            dF*G*H + F*(dG*H + G*dH) + \
              (-1/6) * ((2*F*dF*(C-B)+(dC-dB)*F**2) +
                        (2*G*dG*(A-C)+(dA-dC)*G**2) +
                        (2*H*dH*(B-A)+(dB-dA)*H**2))
        if np.round(1-(I3**2/I2**3),10)==0 :
            xx = 10**20
        else:
            xx = (-1/np.sqrt(1-(I3**2/I2**3)))
        dtetha =  xx * \
            ((dI3*I2**(3/2)-(3/2)*I3*I2**(1/2)*dI2)/I2**3)
        dpsi1 = (a/2) * (3*I2)**(a/2 - 1) * 3 * dI2
        dpsi2 = (2/6) * dtetha * (
            a*(2*np.cos((2*tetha + np.pi)/6))**(a-1) * (-2*np.sin((2*tetha + np.pi)/6)) +
            a*(2*np.cos((2*tetha - 3*np.pi)/6))**(a-1) * (-2*np.sin((2*tetha - 3*np.pi)/6)) +
            a*(-2*np.cos((2*tetha + 5*np.pi)/6))**(a-1) * (+2*np.sin((2*tetha + 5*np.pi)/6)))
        dpsi = dpsi1 * psi2 + dpsi2 * psi1
        grad = (1/a) * (psi / 2) ** (1/a - 1) * (1/2) * dpsi
        #grad = grad / np.linalg.norm(grad)
    else:
        grad = np.zeros(6)
    return phi, grad
def validate():
    # Validating hosford functions
    test_sig = np.random.rand (3,3)
    test_sig = test_sig + np.transpose(test_sig)
    print("hosford0: \n", hosford0(test_sig, 12))
    print("hosford: \n",hosford(test_sig, 12, True))
    # plotting Hosford yield surface with normals
    alphas = np.linspace(0, 2*np.pi, 100)
    zs = np.linspace(-1, 1, 11, endpoint=True)
    nps = len(alphas) * len(zs)
    x, y, z = np.zeros(nps), np.zeros(nps), np.zeros(nps)
    grads = np.zeros((nps,6))
    i = 0
    for zi in zs:
        for alpha in alphas:
            S11, S22 = np.cos(alpha), np.sin(alpha)
            S12 = zi
            sig = np.array([[S11,S12, 0.0],
                           [S12, S22, 0.0],
                           [0.0, 0.0, 0.0]])
            hos, grad = hosford(sig, 40, gradient = True)
            x[i], y[i], z[i] = S11/hos, S22/hos, S12/hos
            grads[i,:] = grad
            i= i+1
    fig1 = plt.figure(figsize=(10, 10))
    ax1 = fig1.add_subplot(projection='3d')
    ax1.scatter(x, y, z)
    ax1.quiver(x, y,z, grads[:,0], grads[:,1], grads[:,3], length=0.2, colors="red")
    grads = np.abs(grads)
    #print(np.average(grads,axis=0))
#validate()

def proportional_loading(nodes, res_dir, model, a=4):
    """Compute and store stress evolution with proportional loading.

    Parameters
    ----------
    nodes : array of size [n_nodes x 3] of type float
        each node defines a direction on which proportional loading is applied
        [S11 , S22, S12]
    res_dir : string
        the directory where the result txt files should be stored.


    """
    gamma = np.linspace(0, 2500e-6, 100)
    phi = np.zeros(len(gamma))
    ref_stress = voce_extended(gamma)
    total_n = len(nodes)
    for i in range(total_n):
        res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
        if False: #os.path.exists(res_file):
            pass
            #print(res_file, " exists!")
        else:
            # -----------------------------------------------------------------
            # building stress tensor
            n_mat = np.array(
                [
                    [nodes[i][0], nodes[i][2], 0.0],
                    [nodes[i][2], nodes[i][1], 0.0],
                    [0.0, 0.0, 0.0],
                ]
            )
            if np.abs(n_mat[0, 0]) != 0:
                n_mat = n_mat / np.abs(n_mat[0, 0])
            n_mat = n_mat / np.linalg.norm(n_mat)
            if model == "von-mises":
                k_steps = ref_stress / mises(n_mat)
            if model == "hosford":
                k_steps = ref_stress / hosford0(n_mat, a)
            sig = np.zeros((len(gamma), 3, 3))
            for j, k_step in enumerate(k_steps):
                sig[j, :, :] = k_step * n_mat
                if model == "von-mises":
                    phi[j] = mises(sig[j, :, :])
                if model == "hosford":
                    phi[j] = hosford0(sig[j, :, :], a)
            # ------------------------------------------------------------------
            # Print results to .txt file with the given job name
            to_write = np.column_stack(
                (
                    sig[:, 0, 0],
                    sig[:, 1, 1],
                    sig[:, 2, 2],
                    sig[:, 0, 1],
                    sig[:, 0, 2],
                    sig[:, 1, 2],
                    phi,
                    gamma,
                )
            )
            np.savetxt(
                res_file,
                to_write,
                delimiter=",",
                fmt="%.5e",
                header="S11, S22, S33, S12, S13, S23, Phi, SDV_GAMMA",
            )