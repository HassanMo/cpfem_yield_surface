"""A module to showcase the features of yield_surface_mesh class.

the example files includes creating a mesh object, applying a proprtional load
using coordinates of nodes in the mesh object, refining mesh using different
refining strategies and finally plotting.
"""

import sys
import os
import numpy as np
from matplotlib import pyplot as plt
import abaqus_emulator as abq
import yield_surface_mesh as ys
import scipy.optimize as opt
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))
os.chdir(os.path.dirname(os.path.abspath(__file__)))
# -----------------------------------------------------------------------------
# --------------------------- D I R E C T O R Y  ------------------------------
# Project Directory
PROJECT_DIR = "TMP_"
# Job Name
JOB_NAME = "YS_EX"
# ------------------------- I N I T I A T I O N  ------------------------------
# Creating a directory for the job
job_dir = f"{PROJECT_DIR}{JOB_NAME}\\".replace("\\", "/")
if not os.path.exists(job_dir):
    os.makedirs(job_dir)
# Creating a directory where the result txt files should be saved
res_dir = f"{job_dir}TXT_Outputs\\".replace("\\", "/")
norm_dir = f"{job_dir}TXT_Normals\\".replace("\\", "/")
if not os.path.exists(res_dir):
    os.makedirs(res_dir)
if not os.path.exists(norm_dir):
    os.makedirs(norm_dir)
# ------------------------- P A R A M E T E R S  ------------------------------
with open("Run_params.txt", "r") as my_file:
    param_line = my_file.readlines()[40:]
pars = []
for i in range(len(param_line)):
    pars.append(list(map(str.strip, param_line[i].split(","))))
# line 1 from param file (initial)
model, n_total, n_equator = pars[0][0], int(pars[0][1]), int(pars[0][2])
# The next lines each hold information for one refining step
refining_modes, n_refining_steps, n_refining_batches = [], [], []
axises, zplanes, phis, ver_phis, autorefines = [], [], [], [], []
for par in pars[1:]:
    refining_modes.append(par[0])
    n_refining_steps.append(int(par[1]))
    n_refining_batches.append(int(par[2]))
    if refining_modes[-1] in ("band_by_plane", "band_by_angle", "vertex"):
        axises.append(np.array([float(par[3]), float(par[4]), float(par[5])]))
    else:
        axises.append(None)
    if refining_modes[-1] in ("band_by_plane"):
        zplanes.append(float(par[6]))
    else:
        zplanes.append(None)
    if refining_modes[-1] in ("band_by_angle"):
        phis.append(np.array([float(par[6]), float(par[7])]))
    else:
        phis.append(None)
    if refining_modes[-1] in ("vertex"):
        ver_phis.append(float(par[6]))
        autorefines.append(int(par[7]))
    else:
        ver_phis.append(None)
        autorefines.append(None)
#
# ------------------------------- B O D Y -------------------------------------
# Initializing mesh generation class
MyMesh = ys.YieldSurfaceMesh()
# -------------------------------> Step 1:
# ---------> Initial mesh:
# Generating the initial mesh structure
MyMesh.initial_sphere_mesh(n_total=n_total, n_equator=n_equator)
# -------------------------------> Step 2:
# ---------> Loading via ABAQUS:
print("--------------------------------------------------->>")
print("-------------------->> Analysing the initial nodes...")
print(f"Total number of loading cases: {MyMesh.n_nodes}")
# abq.proportional_loading(MyMesh.nodes, res_dir)
abq.proportional_loading(np.array(MyMesh.nodes)[:, 0:3], res_dir,
                         "hosford", a=20)
# -------------------------------> Step 3:
# ---------> Initial yield surface:
# Deforming the initial mesh to obtain the yield surface by Updating the
# deformed node coordinates
# Interpolation parameter and level
INTERPOLATE_PARAM = "Phi"
INTERPOLATE_LVL = 112
MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
# initial plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area", savefig="surf0", hide=True)
#
# ------------------------ Refinemenet:
# reading the parameters
image_id = 1
for ri, refining_mode in enumerate(refining_modes):
    # refining_mode is the refinement strategy:
    # "area" or "curvature" or "band_by_plane" or "band_by_angle" or "vertex"
    # Number of refinement steps and max nodes to add in each refining step:
    n_refining_step = n_refining_steps[ri]
    n_refining_batch = n_refining_batches[ri]
    #
    if refining_mode in ("band_by_plane", "band_by_angle", "vertex"):
        # The axis of the refinement band
        axis = axises[ri]
    if refining_mode in ("band_by_plane"):
        # Defines where the axis of the band cuts the band plane
        zplane = zplanes[ri]
    if refining_mode in ("band_by_angle"):
        # Angles that define the band region, in range [0,2pi], where phi1<phi2
        phi1, phi2 = phis[ri][0], phis[ri][1]
    if refining_mode in ("vertex"):
        phi1, phi2 = 0, ver_phis[ri]
        # whether to perfrom autorefine
        autorefine = bool(autorefines[ri])
        normals, n_points = [], []
    #
    for i in range(n_refining_step):
        print(f"---------------------------->> Refining step: {i+1}:")
        if refining_mode in ("curvature", "area"):
            MyMesh.refine_mesh(
                method=refining_mode,
                batch_size=n_refining_batch,
                source_mesh="deformed",
            )
        if refining_mode in ("band_by_plane", "band_by_angle", "vertex"):
            if refining_mode in ("band_by_plane"):
                angle = MyMesh.find_angle_in_deformed_mesh(zplane)
                phi1, phi2 = angle - 1, angle + 1
            if (refining_mode == "vertex" and autorefine):
                normal = MyMesh.normal_in_direction(axis)
                normals.append(normal)
                n_points.append(MyMesh.n_nodes)
            MyMesh.refine_region(
                axis=axis,
                phi1=phi1,
                phi2=phi2,
                batch_size=n_refining_batch,
                source_mesh="deformed",
            )
        # Loading via Abaqus
        print(f"Total number of loading cases: {MyMesh.n_nodes}")
        # abq.proportional_loading(MyMesh.nodes, res_dir)
        abq.proportional_loading(np.array(MyMesh.nodes)[:, 0:3], res_dir,
                         "hosford", a=20)
        # creating the yield surface
        MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
        # saving the plot
        MyMesh.plot_mesh(mode='deformed', color_mode='area',
                         savefig='surf'+str(image_id), hide=True)
        image_id += 1
        if (refining_mode == "vertex" and autorefine):
            # tightening the inspection angle
            n_newnodes = MyMesh.n_nodes - n_points[-1]
            print("number of new nodes added:", n_newnodes)
            if n_newnodes == n_refining_batch:
                phi2 = phi2 / 2
                print("new phi2:", phi2)

            # normal error calculation
            if i >= 1:
                normal_diff = np.abs(normals[-1] - normals[-2])
                print("max error:", max(normal_diff))
                # stop refining if difference is less than one percent
                if max(normal_diff) < 0.002 and max(normal_diff) != 0:
                    break

# final 3D plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area")
#%%
# --------------------------- P L O T T I N G  --------------------------------
# Loading all results, original and interpolations
res_origs, res_interps = [], []
for i in range(MyMesh.n_nodes):
    # Whole original results:
    res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
    res_orig = ys.load_results(res_file, output_mode="original")
    res_origs.append(res_orig)
    # Interpolated levels only:
    # levels = [10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6]
    levels = [112, 112.1, 112.2, 112.3, 112.4]
    res_interp = ys.load_results(
        res_file,
        output_mode="interpolate",
        interp_param="Phi",
        interp_range=levels,
    )
    res_interps.append(res_interp)


# What to plot?
PLOT_ISO_S12_SECTION = False
PLOT_INDIVIDUAL_PATH = False
PLOT_CLOUD = False
PLOT_AUTOREFINE = True

# plotting the  ISO-S12 section
if PLOT_ISO_S12_SECTION:
    ys.plot_section(
        res_interps, MyMesh.elements, section_at=0, mode="2D",
        interp_param="Phi", interp_range=levels)

# --------------> Cloud of points plot, of the whole loading pathes
if PLOT_CLOUD:
    # mode: 'all', 'levels', 'both'
    ys.plot_3d_cloud(res_origs, res_interps, "Phi", mode="both")
#
# --------------> Plots for an individual loading path:
if PLOT_INDIVIDUAL_PATH:
    PATH_INDX = 8
    res_orig = res_origs[PATH_INDX]
    res_interp = res_interps[PATH_INDX]
    # ----> stress/strain and stress ratios/strain curve
    yvars = ["S11", "S22", "S33", "S12", "S13", "S23"]
    ys.plot_xy(res_orig, "Phi", yvars, ylabel="Stress (Mpa)",
               marker=".",
               interp_data=res_interp)

if PLOT_AUTOREFINE:
    print(axises)
    sig = axises[0]
    sig = np.array([[sig[0],sig[2], 0.0],
                   [sig[2], sig[1], 0.0],
                   [0.0, 0.0, 0.0]])
    hos, grad = abq.hosford(sig, 20, gradient = True)
    grad = grad[[0,1,3]]
    grad = grad / np.linalg.norm(grad)
    normals, n_points = np.array(normals), np.array(n_points)
    grads = np.repeat([grad],len(n_points), axis=0)
    fig1, ax1 = plt.subplots(figsize=(8, 6))
    lbls = ["11", "22", "12"]
    clrs = ["red", "green", "blue"]
    for i in range(3):
        label = r"$\frac{\partial \varphi }{\partial \sigma_{"+lbls[i]+"}}$"
        ax1.plot(n_points, normals[:, i], "o-", label = label, color=clrs[i])
        ax1.plot(n_points, grads[:,i], ":", label = "Analytical", color=clrs[i])
    ax1.set_xlabel("number of nodes")
    ax1.set_ylabel("components of the normal vector")
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.2), ncol=3)
    print("Analytical grad:", grad)
    print("Numerical grad:",normals[-1, :])
#%%

def minimizer(a, phi0, sigs, mode):
    sums = 0
    for i, sig in enumerate(sigs):
        if mode == "Quadratic":
            if np.matmul(sig.T, np.matmul(v2f(a), sig)) >= 0:
                phi = np.sqrt(np.matmul(sig.T, np.matmul(v2f(a), sig)))
            else:
                phi = np.matmul(sig.T, np.matmul(v2f(a), sig))/phi0
        if mode == "Linear":
            phi = np.matmul(a, sig)
        sums = sums + (phi0-phi)**2
    return sums


def FindNormal(direction, clr, mode="Linear", hos_a=12, fluctuation=1e-8):
    # print("Finding the gradient in direction:", direction)
    direction = unit_vector(direction)
    directions = []
    directions.append(direction)
    for i in range(3):
        for j in [1, -1]:
            fluc_dir = np.copy(direction)
            fluc_dir[i] = fluc_dir[i] + j*fluctuation
            directions.append(fluc_dir)
    abq.proportional_loading(np.array(directions), norm_dir,
                             "hosford", a=hos_a)
    # Loading all Normals, original and interpolations
    norm_origs, norm_interps_ = [], []
    for i in range(len(directions)):
        # Whole original results:
        norm_file = f"{norm_dir}inp{i+1}.txt".replace("\\", "/")
        norm_orig = ys.load_results(norm_file, output_mode="original")
        norm_origs.append(norm_orig)
        # Interpolated levels only:
        levels = [112]
        norm_interp = ys.load_results(
            norm_file,
            output_mode="interpolate",
            interp_param="Phi",
            interp_range=levels,
        )
        norm_interps_.append(norm_interp)

    # # of nodes, # of levels, # of stored variables
    norm_interps = np.array(norm_interps_)
    sigmas = norm_interps[:, 0, [0, 1, 3]]
    if mode == "Linear":
        params = unit_vector(sigmas[0, :])
    if mode == "Quadratic":
        params = np.array([1, 1, 1, 0, 0, 0])
    res = opt.minimize(minimizer, params, args=(levels[0], sigmas, mode),
                       method="SLSQP")

    if res.success:
        params = res.x
        if mode == "Quadratic":
            #phi = np.sqrt(np.matmul(sigmas[0,:].T, np.matmul(v2f(params), sigmas[0,:])))
            phi = 1
            grad = np.matmul(v2f(params), sigmas[0, :])/phi
        if mode == "Linear":
            grad = params
    else:
        print("Minimization objective function value:", res.fun)
        raise ValueError("Error: Minmizer didn't converge!")
    #"""
    print("----------------------")
    print(res)
    x, y, z = sigmas[:, 0], sigmas[:, 1], sigmas[:, 2]
    ax3.scatter(x, y, z, s=30, c=clr)
    g = unit_vector(grad)
    ax3.quiver(x[0], y[0], z[0], g[0], g[1], g[2], length=0.2, colors=clr)
    #"""
    return unit_vector(grad)


def validate(rn=10, zn=11, hos_a=12, fluctuation = 1e-5,
             ifplot=True, mode="Linear"):
    # mode: "Quadratic", "Linear", "Combined"
    # plotting Hosford yield surface with normals
    alphas = np.linspace(0, 2*np.pi, rn, endpoint=True)
    tethas = np.linspace(0, np.pi, zn, endpoint=True)
    nps = len(alphas) * len(tethas)
    print("Total number of points:", nps)
    x, y, z = np.zeros(nps), np.zeros(nps), np.zeros(nps)
    grads, grads_num = np.zeros((nps, 3)), np.zeros((nps, 3))
    err_angles = np.zeros(nps)
    i = 0
    for tetha in tethas:
        for alpha in alphas:
            # Analytical gradient
            s11, s22, s12 = np.cos(alpha)*np.sin(tetha), np.sin(alpha)*np.sin(tetha), np.cos(tetha)
            hos, grad = abq.hosford(v2f([s11, s22, 0, s12, 0, 0]),
                                    hos_a, gradient=True)
            x[i], y[i], z[i] = s11/hos, s22/hos, s12/hos
            grads[i, :] = unit_vector(grad[[0, 1, 3]])
            # Numerical gradient
            grads_num[i, :] = FindNormal(np.array([s11, s22, s12]),
                                         mode=mode,
                                         hos_a=hos_a,
                                         fluctuation=fluctuation)
            # calculating error angle
            err_angles[i] = np.rad2deg(np.arccos(np.dot(grads[i, :],
                                                        grads_num[i, :])))
            i = i + 1

    if ifplot:
        fig1 = plt.figure(figsize=(10, 10))
        ax1 = fig1.add_subplot(projection='3d')
        p = ax1.scatter(x, y, z, s=30, c=err_angles,
                        cmap=plt.cm.viridis)
        fig1.colorbar(p, ax=ax1)
        #ax1.quiver(x, y, z, grads[:,0], grads[:,1], grads[:,2], length=0.2, colors="red")
        #ax1.quiver(x, y, z, grads_num[:,0], grads_num[:,1], grads_num[:,2], length=0.2, colors="green")
        plt.title(mode)
    av_err, max_err = np.average(err_angles), max(err_angles)
    print("Average Error (deg): ", av_err)
    print("Maximum Error (deg): ", max_err)
    return av_err, max_err


def unit_vector(vector):
    """Return the unit vector of the vector."""
    return vector / np.linalg.norm(vector)


def v2f(v):
    """Convert vector of size 6 in voigt notation to 3x3 symmetric matrix."""
    # order of given vector: a11, a22, a33, a12, a13, a23
    full = np.array([[v[0], v[3], v[4]],
                     [v[3], v[1], v[5]],
                     [v[4], v[5], v[2]]])
    return full


# Validating
fig3 = plt.figure(figsize=(10, 10))
ax3 = fig3.add_subplot(projection='3d')
direction = np.array([1, -0.3, 0.5])
clrs = ["blue", "red"]
for i, fluc in enumerate([1e-2, 1e-4]):
    grad_num = FindNormal(direction, clrs[i], mode="Quadratic", hos_a=12, fluctuation=fluc)
    hos, grad = abq.hosford(v2f([direction[0], direction[1], 0, direction[2], 0, 0]),
                            12, gradient=True)
    grad_an = unit_vector(grad[[0, 1, 3]])
    err_angle = np.rad2deg(np.arccos(np.dot(grad_an, grad_num)))
    #print("grad numerical:", grad_num)
    #print("grad analytical:", grad_an)
    print("error angle:", err_angle)
#%%
# Validating
modes=["Linear", "Quadratic"]
clrs = ["blue", "red"]
flucs = np.logspace(-1, -3, 7)
flucs = [1e-2]
err_angless = []
av_errs = np.zeros((len(modes),len(flucs)))
max_errs = np.zeros((len(modes),len(flucs)))
for j, modej in enumerate(modes):
    for i, fluc in enumerate(flucs):
        print("--------------------------------------")
        print("trying for mode, fluc: ", modej, fluc)
        av_err, max_err = validate(rn=80, zn=31, hos_a=12,
                                   fluctuation=fluc, ifplot=False,
                                   mode=modej)
        av_errs[j,i], max_errs[j,i] = av_err, max_err
        #err_angless.append(err_angles)
"""
err_angless = np.array(err_angless)
err_ang = err_angless[0] - err_angless[1]
err_ang[err_ang < 0], err_ang[err_ang > 0] = -1, 1
fig1 = plt.figure(figsize=(10, 10))
ax1 = fig1.add_subplot(projection='3d')
p = ax1.scatter(x, y, z, s=30, c=err_ang,
                cmap=plt.cm.viridis)
fig1.colorbar(p, ax=ax1)
"""
#%%
fig1, ax1 = plt.subplots(figsize=(8, 6))
for j, modej in enumerate(modes):
    ax1.plot(flucs, av_errs[j,:], "o-", color=clrs[j],
             label=modej+", Average error")
    ax1.plot(flucs, max_errs[j,:], "o--", color=clrs[j],
             label=modej+", Max error")
ax1.set_yscale("log"); ax1.set_xscale("log")
#ax1.set_ylim(1e-3, 1e2)
ax1.set_xlabel("Fluctuation of stress direction, given as a unity vector ()")
ax1.set_ylabel("Angular error (deg) (between Analytical and Numerical) ")
ax1.legend()