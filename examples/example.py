"""A module to showcase the features of yield_surface_mesh class.

the example files includes creating a mesh object, applying a proprtional load
using coordinates of nodes in the mesh object, refining mesh using different
refining strategies and finally plotting.
"""

import sys
import os
import abaqus_emulator as abq

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__),'../')))
os.chdir(os.path.dirname(os.path.abspath(__file__)))
import yield_surface_mesh as ys
# -----------------------------------------------------------------------------
# --------------------------- D I R E C T O R Y  ------------------------------
# Project Directory
PROJECT_DIR = "TMP_"
# Job Name
JOB_NAME = "YS_EX"
# ------------------------- I N I T I A T I O N  ------------------------------
# Creating a directory for the job
job_dir = f"{PROJECT_DIR}{JOB_NAME}\\".replace("\\", "/")
if not os.path.exists(job_dir):
    os.makedirs(job_dir)
# Creating a directory where the result txt files should be saved
res_dir = f"{job_dir}TXT_Outputs\\".replace("\\", "/")
if not os.path.exists(res_dir):
    os.makedirs(res_dir)
# -----------------------------------------------------------------------------
#
# Initializing mesh generation class
MyMesh = ys.YieldSurfaceMesh()
#
# Initial mesh, Generating the initial mesh structure
MyMesh.initial_sphere_mesh(n_total=100, n_equator=0)
#

# Loading via external module:
print("--------------------------------------------------->>")
print(f"Total number of loading cases: {MyMesh.n_nodes}")
abq.proportional_loading(MyMesh.nodes, res_dir)
#
# Initial yield surface:
# Deforming the initial mesh to obtain the yield surface by Updating the
# deformed node coordinates
# Interpolation parameter and level
INTERPOLATE_PARAM = "SDV_GAMMA"
INTERPOLATE_LVL = 2e-3
MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
#
# Refinemenet:
# Refinement strategy: "area" or "curvature" or "region"
REFINING_MODE = "area"
# Number of refinement steps and max nodes to add in each refining step
N_REFINING_STEPS = 4
#
# initial plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area", savefig="surf0", hide=True)


for i in range(N_REFINING_STEPS):
    if REFINING_MODE in ("curvature", "area"):
        MyMesh.refine_mesh(
            method=REFINING_MODE, batch_size=50, source_mesh="deformed"
        )
    if REFINING_MODE == "region":
        # The angles that define the band, in range [0,2pi], where phi1<phi2
        # Its possible to define angles so that the band includes a certain section
        angle = MyMesh.find_angle_in_deformed_mesh(30) # Z Section, here S12
        MyMesh.refine_region(
            axis=[-0.8, -1, 0.1], # The axis of the refinement band
            phi1=angle + 1,
            phi2=angle - 1,
            batch_size=50,
            source_mesh="deformed",
        )
    # Loading via Abaqus
    print(f"---------------------------->> Refining step: {i+1}:")
    print(f"Total number of loading cases: {MyMesh.n_nodes}")
    abq.proportional_loading(MyMesh.nodes, res_dir)
    # creating the yield surface
    MyMesh.deform_mesh(INTERPOLATE_PARAM, INTERPOLATE_LVL, res_dir)
    # saving the plot
    MyMesh.plot_mesh(
        mode="deformed", color_mode="area", savefig="surf" + str(i + 1), hide=True
    )

# final 3D plotting
MyMesh.plot_mesh(mode="deformed", color_mode="area")
#%%
# --------------------------- P L O T T I N G  --------------------------------
# Loading all results, original and interpolations
res_origs, res_interps = [], []
for i in range(MyMesh.n_nodes):
    # Whole original results:
    res_file = f"{res_dir}inp{i+1}.txt".replace("\\", "/")
    res_orig = ys.load_results(res_file, output_mode="original")
    res_origs.append(res_orig)
    # Interpolated levels only:
    levels = [10e-6, 50e-6, 200e-6, 1000e-6, 2000e-6]
    res_interp = ys.load_results(
        res_file,
        output_mode="interpolate",
        interp_param="SDV_GAMMA",
        interp_range=levels,
    )
    res_interps.append(res_interp)


# What to plot?
PLOT_ISO_S12_SECTION = False
PLOT_INDIVIDUAL_PATH = False
PLOT_CLOUD = False

# plotting the  ISO-S12 section
if PLOT_ISO_S12_SECTION:
    ys.plot_section(
        res_interps, MyMesh.elements, section_at=0, mode="2D",
        interp_param="SDV_GAMMA", interp_range=levels)

# --------------> Cloud of points plot, of the whole loading pathes
if PLOT_CLOUD:
    # mode: 'all', 'levels', 'both'
    ys.plot_3d_cloud(res_origs, res_interps, "SDV_GAMMA", mode="both")

#
# --------------> Plots for an individual loading path:
if PLOT_INDIVIDUAL_PATH:
    PATH_INDX = 3
    res_orig = res_origs[PATH_INDX]
    res_interp = res_interps[PATH_INDX]
    # ----> stress/strain and stress ratios/strain curve
    yvars = ["S11", "S22", "S33", "S12", "S13", "S23"]
    ys.plot_xy(res_orig, "SDV_GAMMA", yvars, ylabel="Stress (Mpa)",
               marker=".",
               interp_data=res_interp)
